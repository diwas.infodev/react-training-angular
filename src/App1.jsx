import { useEffect, useState } from "react";
import App2 from "./App2";

const App1 = () => {

  const [state1, setState1] = useState(false);
  const [state2, setState2] = useState(false)

  useEffect(() => {
    //  componentDidMount ==> empty dependency array
    // componentDidUpdate ==> varaibles changes inside dependecy array
    console.log(state1, state2);


    return () => {
      console.log("Component Unmounted")
    }
  }, [state1, state2]);

  const handleSubmit = () => {
    setState1(!state1);
  }

  return (
    <>
      <App2 name={"hari"} />
      <button onClick={() => setState2(!state2)}>chnage State2</button>
    </>
  )
}

export default App1;