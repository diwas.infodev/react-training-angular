import SimpleForm from "./components/SimpleForm";
import useApiRequest from "./useApiRequest";
import { useState } from "./components/SimpleForm"

const App = () => {

  const { data, error, isFetching } = useApiRequest("https://jsonplaceholder.typicode.com/todos");

  return <>
    {/* {isFetching && <span style={{ textAlign: "center" }}>...Loading</span>}
    {error && <span>...Error</span>}
    {data && (
      <ul>
        {data?.map(dat => (
          <li>{dat.title}</li>
        ))}
      </ul>
    )} */}
    <SimpleForm />
  </>
}

export default App;