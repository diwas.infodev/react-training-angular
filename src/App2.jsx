import React, { useEffect, useRef } from 'react'

const App2 = (props) => {

  const ref = useRef(null);

  const { name } = props;

  useEffect(() => {
    ref.current.innerText = "Nepal Nepal";

    return () => {
      console.log("App2 unmounted")
    }
  }, [])

  return (
    <App3 setRef={ref}><div ref={ref} ></div><div>Nepal</div></App3>
  )
}

export default App2;

const App3 = (props) => {
  console.log(props.setRef)
  return <div>App3 <br />{props.children}</div>

}