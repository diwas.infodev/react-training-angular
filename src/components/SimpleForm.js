import React from 'react'
import { useFormik } from "formik"
import * as Yup from "yup"

function SimpleForm() {
  const [formValues, setFormValues] = React.useState({
    name: "",
    email: "",
    address: ""
  });

  // const handleValueChange = (e) => {
  //   setFormValues({
  //     ...formValues,
  //     [e.target.name]: e.target.value
  //   })
  // }

  const onSubmit = (values) => {

  }


  // const validate = (values) => {
  //   let errors = {};
  //   if (!values.name) {
  //     errors.name = "Name is required"
  //   }
  //   if (!values.email) {
  //     errors.email = "Email is required"
  //   }
  //   if (!values.address) {
  //     errors.address = "Address is required"
  //   }
  //   return errors;
  // }

  const validationSchema = Yup.object().shape({
    name: Yup.string().required("Name is required"),
    email: Yup.string().email().required(),
    address: Yup.string().required()
  })




  const formik = useFormik({
    initialValues: formValues,
    onSubmit,
    // validate
    validationSchema
  })



  return (
    <div>
      {console.log("Values", formik.values)}
      {console.log("Errors", formik.errors)}
      {console.log("Touched", formik.touched)}
      <form onSubmit={formik.handleSubmit}>
        <div>
          <label htmlFor="name">Name</label>
          <input type="text" id="name" name="name"
            value={formik.values.name}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            disabled={formik.values.email}
          />
          {formik.errors.name ? formik.touched.name && <span>{formik.errors.name}</span> : null}
        </div>
        <br />
        <div>
          <label htmlFor="email">Email</label>
          <input type="text" id="email" name="email"
            value={formik.values.email}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
          />
          {formik.errors.email ? formik.touched.email && <span>{formik.errors.email}</span> : null}
        </div>
        <br />
        <div>
          <label htmlFor="address">Address</label>
          <input type="text" id="address" name="address"
            value={formik.values.address}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
          />
          {formik.errors.address ? formik.touched.address && <span>{formik.errors.address}</span> : null}
        </div>
        <br />
        <button type="submit">Submit</button>
      </form>
    </div>
  )
}

export default SimpleForm