import axios from "axios";
import { useEffect, useState } from "react";

const useApiRequest = (url) => {

  const [error, setError] = useState(null);
  const [isFetching, setIsFetching] = useState(false);
  const [data, setData] = useState(null);

  const fetchDetails = async () => {
    try {
      setIsFetching(true);
      const response = await axios.get(url);

      if (response.data) {
        setIsFetching(false)
        setData(response.data)
      }
    } catch (err) {
      setIsFetching(false);
      setError(err);
    }
  }


  useEffect(() => {
    fetchDetails();
  }, [url]);

  return { data, isFetching, error }
}

export default useApiRequest;